import { BrowserRouter, Route, Routes } from 'react-router-dom';

import { Home } from './components/Home.jsx';
import { Dashboard } from './components/Dashboard.jsx';
import { Navigation } from './components/Navigation.jsx';
import { AuthProvider } from './context/AuthProvider.jsx';
import { ProtectedRoute } from './components/ProtectedRoute.jsx';
import { Admin } from './components/Admin.jsx';

const App = () => {
  return (
    <BrowserRouter>
      <AuthProvider>
        <h1>React Router</h1>

        <Navigation />

        <Routes>
          <Route index element={<Home />} />
          <Route path="home" element={<Home />} />
          <Route
            path="dashboard"
            element={
              <ProtectedRoute>
                <Dashboard />
              </ProtectedRoute>
            }
          />
          <Route
            path="admin"
            element={
              <ProtectedRoute>
                <Admin />
              </ProtectedRoute>
            }
          />
          <Route path="*" element={<div>No match</div>} />
        </Routes>
      </AuthProvider>
    </BrowserRouter>
  );
};

export default App;
