import * as React from 'react';

import { AuthContext } from './AuthContext.js';

export const useAuth = () => {
  return React.useContext(AuthContext);
};
