import { Navigate, useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';

import { useAuth } from '../context/useAuth.js';

export const ProtectedRoute = ({ children }) => {
  const { token } = useAuth();
  const location = useLocation();

  if (!token) {
    return <Navigate to="/home" replace state={{ from: location }} />;
  }

  return children;
};

ProtectedRoute.propTypes = {
  children: PropTypes.node.isRequired,
};
